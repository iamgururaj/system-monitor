# System Monitor 

### Installation

Postgres database is configured with django - download postgres 11 + from official website and Follow below steps to create database and user for database.
This will be used in django settings

During the Postgres installation, an operating system user named postgres was created to correspond to the postgres PostgreSQL administrative user. We need to change to this user to perform administrative tasks:
```sh
$ sudo su - postgres
```
You should now be in a shell session for the postgres user. Log into a Postgres session by typing:
```sh
$ psql
```
First, we will create a database for our Django project. Each project should have its own isolated database for security reasons. We will call our database system_monitor in this guide, but it's always better to select something more descriptive:
```sh
$ CREATE DATABASE system_monitor;
```
Remember to end all commands at an SQL prompt with a semicolon.

Next, we will create a database user which we will use to connect to and interact with the database. Set the password to something strong and secure:
```sh
$ CREATE USER system_monitor_user WITH PASSWORD 'system_monitor_password';
```
Afterwards, we'll modify a few of the connection parameters for the user we just created. This will speed up database operations so that the correct values do not have to be queried and set each time a connection is established.

We are setting the default encoding to UTF-8, which Django expects. We are also setting the default transaction isolation scheme to "read committed", which blocks reads from uncommitted transactions. Lastly, we are setting the timezone. By default, our Django projects will be set to use UTC:
```sh
$ ALTER ROLE system_monitor_user SET client_encoding TO 'utf8';
$ ALTER ROLE system_monitor_user SET default_transaction_isolation TO 'read committed';
$ ALTER ROLE system_monitor_user SET timezone TO 'UTC';
```
Now, all we need to do is give our database user access rights to the database we created:
```sh
$ GRANT ALL PRIVILEGES ON DATABASE system_monitor TO system_monitor_user;
```
Exit the SQL prompt to get back to the postgres user's shell session:

```sh
$ \q
```
Exit out of the postgres user's shell session to get back to your regular user's shell session:
```sh
$ Exit
```

License
----

MIT



